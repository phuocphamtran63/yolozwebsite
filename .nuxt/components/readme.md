# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<CommonBody>` | `<common-body>` (components/Common/Body.vue)
- `<CommonCarousel>` | `<common-carousel>` (components/Common/Carousel.vue)
- `<CommonTemplate>` | `<common-template>` (components/Common/CommonTemplate.vue)
- `<CommonFooter>` | `<common-footer>` (components/Common/Footer.vue)
- `<CommonHeader>` | `<common-header>` (components/Common/Header.vue)
- `<CommonIndexMainMenu>` | `<common-index-main-menu>` (components/Common/IndexMainMenu.vue)
- `<CommonMainContent>` | `<common-main-content>` (components/Common/MainContent.vue)
- `<CommonNavbar>` | `<common-navbar>` (components/Common/Navbar.vue)
- `<CommonWrapper>` | `<common-wrapper>` (components/Common/Wrapper.vue)
- `<CommonStats>` | `<common-stats>` (components/Common/stats.vue)
