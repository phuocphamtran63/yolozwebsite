export { default as CommonBody } from '../../components/Common/Body.vue'
export { default as CommonCarousel } from '../../components/Common/Carousel.vue'
export { default as CommonTemplate } from '../../components/Common/CommonTemplate.vue'
export { default as CommonFooter } from '../../components/Common/Footer.vue'
export { default as CommonHeader } from '../../components/Common/Header.vue'
export { default as CommonIndexMainMenu } from '../../components/Common/IndexMainMenu.vue'
export { default as CommonMainContent } from '../../components/Common/MainContent.vue'
export { default as CommonNavbar } from '../../components/Common/Navbar.vue'
export { default as CommonWrapper } from '../../components/Common/Wrapper.vue'
export { default as CommonStats } from '../../components/Common/stats.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
